<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/" name="xsl:initial-template">
	<!-- This looks good, Jody.
	10/10
	-->
	<html>
		<head>
			<title>Assignment 3</title>
		</head>
		<body>
		<h4>Customer Info</h4>
		Name: <xsl:value-of select="telephoneBill/customer/name"/><br/>
		Address: <xsl:value-of select="telephoneBill/customer/address"/><br/>
		City: <xsl:value-of select="telephoneBill/customer/city"/><br/>
		Province: <xsl:value-of select="telephoneBill/customer/province"/><br/>
			<table border="1" cellpadding="3" cellspacing="3">
					<tbody>
						<tr>
							<th>Call Number</th>
							<th>Date</th>
							<th>Duration In Minutes</th>
							<th>Charge</th>
						</tr>
					<xsl:for-each select="telephoneBill/call">
					<tr>
						<td><xsl:value-of select="@number"/></td>
						<td><xsl:value-of select="@date"/></td>
						<td><xsl:value-of select="@durationInMinutes"/></td>
						<td><xsl:value-of select="@charge"/></td>
					</tr>
					</xsl:for-each>
						
					</tbody>
				</table>
		</body>
	</html>
	</xsl:template>
</xsl:stylesheet>
